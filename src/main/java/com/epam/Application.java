package com.epam;

import java.util.Scanner;

/**
 * @author Olexandra Stan
 * @version 1.0
 */

public class Application {
    public static void main(String[] args) {
        System.out.println("Hello");

        int a;
        int b;
        int sumOdd = 0;
        int sumEven = 0;

        Scanner s = new Scanner(System.in);
        System.out.print("Enter the start:");
        a = s.nextInt();
        System.out.print("Enter the end:");
        b = s.nextInt();

        int maxOdd;
        int maxEven;
        if((b % 2) == 0) {
          maxEven = b;
          maxOdd = b - 1;
        } else {
            maxEven = b - 1;
            maxOdd = b;
        }
        System.out.print("Odd numbers:");
        for(int i = a ; i < b ; i++) {
            if((i % 2) != 0) {
                System.out.print(i +" ");
                sumOdd += i ;
            }
        }

        System.out.print("\nEven numbers:");
        for(int i = b ; i > a ; i--) {
            if((i % 2) == 0) {
                System.out.print(i +" ");
                sumEven += i ;
            }
        }

        System.out.print("\nSum of odd numbers: "+ sumOdd);
        System.out.print("\nSum of even numbers: "+ sumEven);

        System.out.print("\n\nEnter  n (amount of fibonacci numbers):");
        int n = s.nextInt();

        fibonacciFunc(maxOdd, maxEven, n);
    }

    /**
     * This method is designed to count Fibonacci numbers.
     * @param fib1  initial value of first Fibonacci number
     * @param fib2  initial value of second Fibonacci number
     * @param size  amount of Fibonacci numbers , entered by user
     */

    private static void fibonacciFunc(int fib1, int fib2, int size) {
        int countEvenFib=1;
        int countOddFib=1;
        int fib3=0;
        System.out.print(fib1 + " ");
        System.out.print(fib2 + " ");
        for (int i = 2; i < size; ++i) {
            fib3 = fib1 + fib2;
            fib1 = fib2;
            fib2 = fib3;
            System.out.print(fib3 + " ");
            if ((fib3 % 2) == 0) {
                countEvenFib++;
            } else if ((fib3 % 2) == 1) {
                countOddFib++;
            }
        }
        final int coef=100;
        double perEvenFib = (double) (countEvenFib * coef) / (size);
        double perOddFib = (double) (countOddFib * coef) / (size);

        System.out.print("\n");
        System.out.println("Percentage of even fibonacci numbers = " + perEvenFib + '%');
        System.out.println("Percentage of odd fibonacci numbers = " + perOddFib + '%');
    }
}
